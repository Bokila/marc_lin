[![Build Status](https://drone.948794crazy.info/api/badges/Bokila/marc_lin/status.svg)](https://drone.948794crazy.info/Bokila/marc_lin)

# Restore Postgresql DB

## step 1

```
> docker-compose up -d
```

## step 2

```
> docker-compose stop
```

## step 3

```
> docker run --rm --volumes-from marc_lin_db_1 -v $(pwd):/backup ubuntu
  bash -c "cd /var/lib/postgresql/data && tar xvf /backup/data.tar --strip 1"
```

## step 4

```
> docker-compose start
```

# Because some weired beavior of docker, after chagne css file need to manually copy into docker container

```
## docker cp staticfiles/ marclin_web_1:/code/
```
