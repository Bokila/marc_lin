from django.http import HttpResponsePermanentRedirect


class WwwRedirectMiddleware:
    """Drop www
    https://adamj.eu/tech/2020/03/02/how-to-make-django-redirect-www-to-your-bare-domain/

    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        host = request.get_host().partition(":")[0]

        if host.startswith("www"):
            redirect_url = f"http://{host.replace('www.', '')}{request.path}"

            return HttpResponsePermanentRedirect(redirect_url)

        return self.get_response(request)
