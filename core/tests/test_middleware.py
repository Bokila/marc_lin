from http import HTTPStatus

import pytest
from django.test import RequestFactory

from core.middleware import WwwRedirectMiddleware

DUMMY_RESPONSE = object()


@pytest.fixture
def www_redirect_middleware():
    dummy_response = DUMMY_RESPONSE

    return WwwRedirectMiddleware(lambda request: dummy_response)


class TestWwwRedirectMiddleware:
    def test_www_redirect(self, www_redirect_middleware):
        request = RequestFactory().get("/", HTTP_HOST="www.948794crazy.info")
        response = www_redirect_middleware(request)
        assert response.status_code == HTTPStatus.MOVED_PERMANENTLY
        assert response["Location"] == "http://948794crazy.info/"

    def test_non_redirect(self, www_redirect_middleware):
        request = RequestFactory().get("/", HTTP_HOST="948794crazy.info")
        response = www_redirect_middleware(request)
        assert response is DUMMY_RESPONSE
