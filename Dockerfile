FROM python:3.8.3-slim-buster AS compile-image

ARG CURRENT_ENV="dev"

ENV POETRY_VERSION=1.0.0

RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends gcc python3-dev libpq-dev \
    && pip install --no-cache-dir -U pip setuptools wheel "poetry==$POETRY_VERSION"

ENV VIRTUAL_ENV=/opt/venv
RUN python -m venv $VIRTUAL_ENV
# Make sure we use the virtualenv:
ENV PATH="/opt/venv/bin:$PATH"
COPY pyproject.toml poetry.lock ./

RUN poetry install $(test $CURRENT_ENV = production && echo --no-dev) --no-ansi --no-root


FROM python:3.8.3-slim-buster as production

# Turns off writing .pyc files; superfluous on an ephemeral container.
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get update \
    && apt-get install -y --no-install-recommends libpq-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*
 
COPY --from=compile-image /opt/venv /opt/venv

# Make sure we use the virtualenv:
ENV PATH="/opt/venv/bin:$PATH"
ENV TINI_VERSION="v0.19.0"
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini \
    && useradd -m -r appuser # Create home directory for system user
USER appuser

WORKDIR /home/appuser

COPY --chown=appuser:appuser . .
RUN mkdir logs

EXPOSE 8888
ENTRYPOINT ["/tini", "--"]
