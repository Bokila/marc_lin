from fabric import Connection
from invoke import task

PROJECT_DIR = "/root/Projects/marc_lin"
CONN = Connection(host="128.199.213.157", user="root")


@task
def push(local):
    """Push to repo"""
    local.run("git push")


@task
def pull(local):
    """Pull repo on server"""
    CONN.run(f"cd {PROJECT_DIR}; git pull")


@task
def push_pull(local):
    """Push to repo & pull on server"""
    push(local)
    pull(local)


@task
def backup_db(local):
    """Backup local database"""
    local.run(
        "docker run --rm --volumes-from marc_lin_db_1 -v $(pwd):/backup ubuntu tar -cvf /backup/data.tar -C /var/lib/postgresql/data ."
    )


@task
def test(local):
    """Run pytest in container"""
    local.run("docker-compose exec web pytest", pty=True)


@task
def drone(local):
    """Run CI/CD from local cli"""
    local.run("drone exec --secret-file .drone_cli.secret")


@task
def backup_from_server(local):
    """Backup database on server & scp to local"""
    CONN.run(
        "docker run --rm --volumes-from marc_lin_db_1 -v /root/Projects/marc_lin/:/backup ubuntu \
        tar -cvf /backup/data.tar -C /var/lib/postgresql/data ."
    )
    local.run("scp root@128.199.213.157:/root/Projects/marc_lin/data.tar .")


@task
def restore_db(local):
    local.run(
        "docker run --rm --volumes-from marc_lin_db_1 -v $(pwd):/backup ubuntu bash -c 'cd /var/lib/postgresql/data && tar xvf /backup/data.tar --strip 1'"
    )
