"""
WSGI config for marc_lin project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os

from decouple import config
from django.core.wsgi import get_wsgi_application
from django.db.backends.signals import connection_created
from django.dispatch import receiver

os.environ.setdefault(
    "DJANGO_SETTINGS_MODULE",
    config("DJANGO_SETTINGS_MODULE", default="marc_lin.settings.settings"),
)

application = get_wsgi_application()


@receiver(connection_created)
def setup_postgres(connection, **kwargs):
    """
    setting a timeout on SQL queries
    (https://python.works-hub.com/learn/9-django-tips-for-working-with-databases-f48d8)
    """
    if connection.vendor != "postgresql":
        return

    # Timeout statements after 30 seconds.
    with connection.cursor() as cursor:
        cursor.execute("SET statement_timeout To 30000;")
