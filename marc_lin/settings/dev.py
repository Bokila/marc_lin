from .settings import *


# customized show_toolbar logic
def show_toolbar(request):
    return True if DEBUG else False


DEBUG_TOOLBAR_CONFIG = {"SHOW_TOOLBAR_CALLBACK": show_toolbar}
# config for debug_toolbar, ip of {{request.META.REMOTE_ADDR}}
# INTERNAL_IPS = [
#     '172.19.0.2',
# ]
if DEBUG:
    MIDDLEWARE += ["debug_toolbar.middleware.DebugToolbarMiddleware", ]

    INSTALLED_APPS += ["debug_toolbar", "django_extensions"]
