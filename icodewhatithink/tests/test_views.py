from django.test import RequestFactory
from icodewhatithink import views


class TestHomeView:
    def test_home(self):
        req = RequestFactory().get("/")
        resp = views.HomeView.as_view()(req)

        assert resp.status_code == 200, "Shoud return a response successfully"
