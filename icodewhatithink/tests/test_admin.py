import pytest
from django.contrib.admin.sites import AdminSite
from icodewhatithink import admin, models
from mixer.backend.django import mixer

pytestmark = pytest.mark.django_db


class TestPostAdmin:
    def test_show_tags(self):
        site = AdminSite()
        post_page_down_admin = admin.PostPageDownAdmin(models.Post, site)
        tag1 = mixer.blend("icodewhatithink.Tag")
        tag2 = mixer.blend("icodewhatithink.Tag")
        obj = mixer.blend("icodewhatithink.Post", tags=[tag1, tag2])

        result = post_page_down_admin.show_tags(obj)
        expected = "/".join([tag.name for tag in obj.tags.all()])
        assert result == expected, "Should show all the tags of the post"
