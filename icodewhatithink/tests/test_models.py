import pytest
from mixer.backend.django import mixer

pytestmark = pytest.mark.django_db  # In order to save the data into the database


class TestPost:
    def test_init(self):
        obj = mixer.blend("icodewhatithink.Post")

        assert obj.pk == 1, "Should save an post instance"

    def test_str(self):
        obj = mixer.blend("icodewhatithink.Post")
        assert str(obj) == obj.title

    def test_get_absolute_url(self):
        obj = mixer.blend("icodewhatithink.Post")
        assert obj.get_absolute_url() == "/{slug}/".format(slug=obj.slug)


class TestTag:
    def test_init(self):
        obj = mixer.blend("icodewhatithink.Tag")

        assert obj.pk == 1, "Should save an tag instance"

    def test_str(self):
        obj = mixer.blend("icodewhatithink.Tag")
        assert str(obj) == obj.name
