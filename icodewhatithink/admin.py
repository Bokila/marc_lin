from typing import Set

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.contrib.flatpages.admin import FlatPageAdmin
from django.contrib.flatpages.models import FlatPage
from django.db import models
from pagedown.widgets import AdminPagedownWidget

from .models import Post, Tag

admin.site.unregister(User)
admin.site.unregister(FlatPage)


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    readonly_fields = ("last_login", "date_joined")

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        is_superuser = request.user.is_superuser
        disabled_fields: Set[str] = set()

        # fields can only be update by superuser
        if not is_superuser:
            disabled_fields |= {"username", "is_superuser", "user_permissions"}

        # Prevent non-superusers from editing their own permissions
        if not is_superuser and obj is not None and obj == request.user:
            disabled_fields |= {
                "is_staff",
                "is_superuser",
                "groups",
                "user_permissions",
            }

        for f in disabled_fields:
            if f in form.base_fields:
                form.base_fields[f].disabled = True

        return form

    def has_delete_permission(self, request, obj=None):
        is_superuser = request.user.is_superuser
        if not is_superuser:
            return False
        return True


@admin.register(FlatPage)
class FlatPageDownAdmin(FlatPageAdmin):
    formfield_overrides = {models.TextField: {"widget": AdminPagedownWidget}}


@admin.register(Post)
class PostPageDownAdmin(admin.ModelAdmin):
    list_display = ("title", "pub_datetime", "show_tags", "show")
    list_editable = ("pub_datetime", "show")
    save_on_top = True
    prepopulated_fields = {"slug": ("title",)}
    formfield_overrides = {models.TextField: {"widget": AdminPagedownWidget}}

    def show_tags(self, obj):
        return "/".join([tag.name for tag in obj.tags.all()])


@admin.register(Tag)
class TagPageDownAdmin(admin.ModelAdmin):
    pass
