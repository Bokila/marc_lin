import django_filters
import graphene
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType
from graphql_relay import from_global_id

from .models import Post, Tag


class PostFilter(django_filters.FilterSet):
    class Meta:
        model = Post
        fields = {
            "title": ["exact", "icontains", "istartswith"],
            "show": ["exact"],
            "tags": ["exact", "icontains", "istartswith"],
        }

    order_by = django_filters.OrderingFilter(fields=(("pub_datetime", "pub_datetime"),))


class PostNode(DjangoObjectType):
    class Meta:
        model = Post
        interfaces = (graphene.relay.Node,)


class TagNode(DjangoObjectType):
    class Meta:
        model = Tag
        filter_fields = {"name": ["exact", "icontains", "istartswith"]}
        interfaces = (graphene.relay.Node,)


class Query(graphene.ObjectType):
    all_posts = DjangoFilterConnectionField(PostNode, filterset_class=PostFilter)
    all_tags = DjangoFilterConnectionField(TagNode)


class UpdateTagMutation(graphene.relay.ClientIDMutation):
    tag = graphene.Field(TagNode)

    class Input:
        id = graphene.ID()
        name = graphene.String(required=True)

    def mutate_and_get_payload(root, info, **input):
        user = info.context.user
        if user.is_anonymous:
            raise Exception("You must be logged in to update!")
        tag = Tag.objects.get(pk=from_global_id(input.get("id"))[1])
        tag.name = input.get("name")
        tag.save()
        return UpdateTagMutation(tag=tag)


class Mutation:
    update_tag = UpdateTagMutation.Field()
