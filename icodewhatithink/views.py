from django.views import generic

from .models import Post


class HomeView(generic.ListView):
    model = Post
    queryset = Post.objects.showed()
    context_object_name = "posts"
    template_name = "icodewhatithink/home.html"


class PostDetailView(generic.DetailView):
    model = Post
    slug_field = "slug"
    template_name = "icodewhatithink/post.html"
