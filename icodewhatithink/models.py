from django.db import models
from django.urls import reverse
from django.utils import timezone


class Tag(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return self.name


class PostQuerySet(models.QuerySet):
    def showed(self):
        return self.filter(show=True).only("title", "slug")


class Post(models.Model):
    title = models.CharField(max_length=256)
    slug = models.SlugField(max_length=256, null=True)
    author = models.CharField(max_length=256, default="Sentoz Lin")
    content = models.TextField()
    pub_datetime = models.DateTimeField(default=timezone.now)
    tags = models.ManyToManyField(Tag, blank=True, related_name="posts")
    show = models.BooleanField(default=True)

    objects = PostQuerySet.as_manager()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("icodewhatithink:post_detail", kwargs={"slug": self.slug})

    class Meta:
        ordering = ("-pub_datetime",)
