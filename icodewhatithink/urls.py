from django.urls import path

from . import views

app_name = 'icodewhatithink'
urlpatterns = [
    path('', views.HomeView.as_view(), name="home"),
    path('<slug:slug>/', views.PostDetailView.as_view(), name="post_detail"),
]
