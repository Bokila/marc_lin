from django.urls import path

from . import views

app_name = 'privacy_thinking'
urlpatterns = [
    path('', views.posts, name="posts"),
    # path('<slug:slug>/', views.PostDetailView.as_view(), name="post_detail"),
]
