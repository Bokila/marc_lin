from django.apps import AppConfig


class PrivacyThinkingConfig(AppConfig):
    name = 'privacy_thinking'
