from django.shortcuts import render
from django.template.response import TemplateResponse

# Create your views here.
def posts(request):
    return TemplateResponse(request, "privacy_thinking/posts.html")
