#! /bin/sh
python manage.py collectstatic --noinput --clear
gunicorn marc_lin.wsgi:application \
    --reload \
    --bind :8888 \
    --timeout 3600 \
    --workers 2 \
    --threads 4 \
    --worker-class gthread \
    --worker-tmp-dir /dev/shm \
    --max-requests 1000 \
    --max-requests-jitter 50 \
    --access-logfile /home/appuser/logs/access.log \
    --error-logfile /home/appuser/logs/error.log \
    --capture-output
